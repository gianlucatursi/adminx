import Locator from '../locator/locator';

export default class AbstractController{

  constructor(){
    this._bus = Locator.resolve(Locator.types.BUS);
    this._logger = Locator.resolve(Locator.types.LOGGER);

    this._bus.register(this._bus.events.APP_LOADED, this.onAppLoaded);

  }

  onAppLoaded(){}

}