import AbstractController from './abstract.js';

export default class MainController extends AbstractController{
  /**
   * this._bus for events
   */
  constructor($scope){
    super();

    this.title = 'Ciao!';
    this._scope = $scope;

  }

}