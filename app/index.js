import angular from 'angular';
import MainController from './controllers/main';
import Locator from './locator/locator';

Locator.init();

angular.module('adminx.controllers', []);
angular.module('adminx.services', []);
angular.module('adminx.directives', []);

const APP = angular.module('adminx', ['adminx.controllers', 'adminx.services', 'adminx.directives']);

MainController.$inject = ['$scope'];

APP.run(()=>{
  const _bus = Locator.resolve(Locator.types.BUS);

  _bus.post(_bus.events.APP_LOADED);
  
});

APP.controller('AdminX',  MainController);
