import Services from './services/services';
import Eventbus from './services/bus/eventbus';
import LogService from './services/log_service';

const services = {};

/**
 * Service Locator - Singleton
 */
class Locator {

  constructor() {
    return Locator;
  }

  /**
   * static - list of all types
   *
   * @return {type}  description
   */
  static get types() {
    return Services.types;
  }

  /**
   * static - register a service
   *
   * @param  {Services.types} symbol  description
   * @param  {Object} service description
   */
  static register(symbol, service) {

    if (!Services.isValid(symbol)) {
      throw new Error('You cannot register a service with an INVALID symbol ' + (symbol || 'undefined').toString());
    }

    services[symbol] = service;
  }


  /**
   * static - resolve a service
   *
   * @param  {Services.types} symbol description
   * @return {type}        description
   */
  static resolve(symbol) {
    if (!Services.isValid(symbol)) {
      throw new Error('You cannot resolve a service with an INVALID symbol ' + (symbol || 'undefined').toString());
    }

    return services[symbol];
  }


  /**
   * static - Used only for testing [reset the Singleton]
   *
   */
  static _reset() {
    for (var key in services) {
      delete services[key];
    }
  }

  /**
   * Initialize Locator with core services.
   *
   */
  static init() {
    Locator.register(Locator.types.BUS, new Eventbus());
    Locator.register(Locator.types.LOGGER, new LogService());
  }

}

export default Locator;