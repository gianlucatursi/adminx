const Events = {
  DEFAULT: Symbol("default"),
  APP_LOADED : Symbol("app_loaded")
};

export default Events;
