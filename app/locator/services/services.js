const types = {
  BUS: Symbol('Eventbus'),
  LOGGER: Symbol('Logger'),
};

const reverse = {};
for (let prop in types) {
  reverse[types[prop]] = prop;
}

class Services {

  static get types() {
    return types;
  }

  /**
   * static - Check if a symbol is valid and is implementable service
   *
   * @param  {type} symbol description
   * @return {boolean}        description
   */
  static isValid(symbol) {
    return symbol in reverse;
  }

}

export default Services;
