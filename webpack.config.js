var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var extractCSS = new ExtractTextPlugin('[name].bundle.css');

module.exports = {
  entry: './app/index.js',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/app'
  },
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        use: "eslint-loader",
        exclude: /node_modules/
      },
      {
        test: /\.(css|scss)$/i,
        exclude: /node_modules/,
        use: extractCSS.extract({
          fallback: "style-loader",
          use: ['css-loader', 'sass-loader']
        })
      },
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      minChunks: 2,
      name: 'vendor'
    }),
    extractCSS
  ]
};